package com.baomidou.crab.sys.mapper;

import com.baomidou.crab.sys.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统用户表 Mapper 接口
 * </p>
 *
 * @author jobob
 * @since 2018-09-16
 */
public interface UserMapper extends BaseMapper<User> {

}
